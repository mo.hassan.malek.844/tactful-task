FROM  node:13.8.0
COPY . .
RUN npm i cors express nodemon
RUN npm install dotenv
#RUN npm ci --only=production
CMD ["node","server.js"]
